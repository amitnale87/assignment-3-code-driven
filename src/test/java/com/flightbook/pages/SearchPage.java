package com.flightbook.pages;

import java.util.List;

import org.openqa.selenium.By;

import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.testng.report.Report;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class SearchPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "searchResultList")
	private List<QAFWebElement> searchResultList;
	@FindBy(locator = "lowestPrice")
	private QAFWebElement lowestPrice;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public List<QAFWebElement> getSearchResultList() {
		return searchResultList;
	}

	public QAFWebElement getLowestPrice() {
		return lowestPrice;
	}
	
	public void displaySearchResult()
	{
		Reporter.log("The Count of avialable flight is "+searchResultList.size());
		for (int i=0; i<searchResultList.size();i++)
		{
			
			//String pathForPriceOfEachFilght = searchResultList.get(i)+" span[class=\"dollars price-emphasis\"]";
			//String path=pathForPriceOfEachFilght.substring(16);
			//String priceOfEachFilght = driver.findElement(By.cssSelector(path)).getText();
			//Reporter.log("The Price of the flights are : "+priceOfEachFilght,true);
			//Reporter.log("The Price of the flights are : "+path,true);
			Reporter.log("The Price of the flights are: "+searchResultList.get(i).findElement(By.cssSelector("span[class=\"dollars price-emphasis\"]")).getText());
			
		}
		Reporter.log("\n");
	}

}
