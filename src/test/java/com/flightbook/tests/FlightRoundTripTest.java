package com.flightbook.tests;

import org.testng.annotations.Test;

import com.flightbook.pages.HomePage;
import com.flightbook.pages.SearchPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class FlightRoundTripTest extends WebDriverTestCase {

	@Test
	public void testRoundTrip()
	{
		HomePage homepage = new HomePage();
		homepage.launchPage();
		homepage.bookFlight();
		
		SearchPage searchpage = new SearchPage();
		searchpage.displaySearchResult();
	}
}
